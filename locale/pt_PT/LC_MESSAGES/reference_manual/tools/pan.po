msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 14:29+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: toolpan icons Krita ref image Kritamouseleft\n"
"X-POFile-SpellExtra: Kritamousemiddle kbd navigation images mousemiddle\n"
"X-POFile-SpellExtra: alt pantool mouseleft\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: botão esquerdo"

#: ../../<rst_epilog>:6
msgid ""
".. image:: images/icons/Krita_mouse_middle.png\n"
"   :alt: mousemiddle"
msgstr ""
".. image:: images/icons/Krita_mouse_middle.png\n"
"   :alt: botão do meio"

#: ../../<rst_epilog>:80
msgid ""
".. image:: images/icons/pan_tool.svg\n"
"   :alt: toolpan"
msgstr ""
".. image:: images/icons/pan_tool.svg\n"
"   :alt: posicionamento"

#: ../../reference_manual/tools/pan.rst:1
msgid "Krita's pan tool reference."
msgstr "A referência da ferramenta de posicionamento do Krita."

#: ../../reference_manual/tools/pan.rst:11
msgid "Tools"
msgstr "Ferramentas"

#: ../../reference_manual/tools/pan.rst:11
msgid "Pan"
msgstr "Deslocamento"

#: ../../reference_manual/tools/pan.rst:16
msgid "Pan Tool"
msgstr "Ferramenta de Posicionamento"

#: ../../reference_manual/tools/pan.rst:18
msgid "|toolpan|"
msgstr "|toolpan|"

#: ../../reference_manual/tools/pan.rst:20
msgid ""
"The pan tool allows you to pan your canvas around freely. It can be found at "
"the bottom of the toolbox, and you just it by selecting the tool, and doing |"
"mouseleft| :kbd:`+ drag` over the canvas."
msgstr ""
"A ferramenta de posicionamento permite-lhe deslocar-se ou posicionar-se na "
"sua área de desenho à vontade. Podê-la-á encontrar na parte inferior da área "
"de ferramentas e poderá usá-la se seleccionar a ferramenta e usar o |"
"mouseleft| + :kbd:`arrastamento` da área de desenho."

#: ../../reference_manual/tools/pan.rst:22
msgid ""
"There are two hotkeys associated with this tool, which makes it easier to "
"access from the other tools:"
msgstr ""
"Existem duas combinações de teclas associadas a esta ferramenta, o que "
"simplifica o acesso a partir das outras ferramentas:"

#: ../../reference_manual/tools/pan.rst:24
msgid ":kbd:`Space +` |mouseleft| :kbd:`+ drag` over the canvas."
msgstr ""
":kbd:`Espaço +` |mouseleft| + :kbd:`arrastamento` pela área de desenho."

#: ../../reference_manual/tools/pan.rst:25
msgid "|mousemiddle| :kbd:`+ drag` over the canvas."
msgstr "|mousemiddle| + :kbd:`arrastamento` pela área de desenho."

#: ../../reference_manual/tools/pan.rst:27
msgid "For more information on such hotkeys, check :ref:`navigation`."
msgstr ""
"Para mais informações sobre essas combinações de teclas, consulte a :ref:"
"`navigation`."
