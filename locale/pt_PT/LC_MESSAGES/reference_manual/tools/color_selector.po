msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 10:32+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: mixingcolors en Krita guilabel image kbd images ref\n"
"X-POFile-SpellExtra: ColorDropperToolOptions tools\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../../reference_manual/tools/color_selector.rst:1
msgid "Krita's color selector tool reference."
msgstr "Referência à ferramenta de selecção de cores do Krita."

#: ../../reference_manual/tools/color_selector.rst:12
msgid "Tools"
msgstr "Ferramentas"

#: ../../reference_manual/tools/color_selector.rst:12
msgid "Colors"
msgstr "Cores"

#: ../../reference_manual/tools/color_selector.rst:12
msgid "Eyedropper"
msgstr "Conta-Gotas"

#: ../../reference_manual/tools/color_selector.rst:12
msgid "Color Selector"
msgstr "Selecção de Cores"

#: ../../reference_manual/tools/color_selector.rst:17
msgid "Color Selector Tool"
msgstr "Ferramenta de Selecção de Cores"

#: ../../reference_manual/tools/color_selector.rst:20
msgid ""
"This tool allows you to choose a point from the canvas and make the color of "
"that point the active foreground color. When a painting or drawing tool is "
"selected the Color Picker tool can also be quickly accessed by pressing the :"
"kbd:`Ctrl` key."
msgstr ""
"Esta ferramenta permite-lhe escolher um dado ponto na área de desenho e "
"fazer com que a cor desse ponto fique a cor principal activa nesse momento. "
"Quando for seleccionada uma ferramenta de pintura ou desenho, também poderá "
"aceder rapidamente à ferramenta de Selecção de Cores se carregar em :kbd:"
"`Ctrl`."

#: ../../reference_manual/tools/color_selector.rst:23
msgid ".. image:: images/tools/Color_Dropper_Tool_Options.png"
msgstr ".. image:: images/tools/Color_Dropper_Tool_Options.png"

#: ../../reference_manual/tools/color_selector.rst:24
msgid ""
"There are several options shown in the :guilabel:`Tool Options` docker when "
"the :guilabel:`Color Picker` tool is active:"
msgstr ""
"Existem várias opções visíveis na área de :guilabel:`Opções da Ferramenta` "
"quando estiver activa a ferramenta de :guilabel:`Selecção de Cores`:"

#: ../../reference_manual/tools/color_selector.rst:26
msgid ""
"The first drop-down box allows you to select whether you want to sample from "
"all visible layers or only the active layer. You can choose to have your "
"selection update the current foreground color, to be added into a color "
"palette, or to do both."
msgstr ""
"A primeira lista permite-lhe seleccionar se deseja criar uma amostra de "
"todas as camadas visíveis ou apenas da camada activa. Poderá optar por ter a "
"sua selecção a actualizar a cor principal actual, a adicionar a mesma a uma "
"paleta de cores ou fazer ambas as coisas."

#: ../../reference_manual/tools/color_selector.rst:30
msgid ""
"The middle section contains a few properties that change how the Color "
"Picker picks up color; you can set a :guilabel:`Radius`, which will average "
"the colors in the area around the cursor, and you can now also set a :"
"guilabel:`Blend` percentage, which controls how much color is \"soaked up\" "
"and mixed in with your current color. Read :ref:`mixing_colors` for "
"information about how the Color Picker's blend option can be used as a tool "
"for off-canvas color mixing."
msgstr ""
"A secção do meio contém algumas propriedades que mudam a forma como o "
"Selector de Cores escolhe a cor; poderá definir um :guilabel:`Raio`, o qual "
"irá calcular a média de todas as cores na área em torno do cursor, assim "
"como definir uma percentagem da :guilabel:`Mistura`, que controla a "
"quantidade de cor \"encharcada\" e misturada com a sua cor actual. Consulte "
"o :ref:`mixing_colors` para obter mais informações sobre a forma como a "
"opção de mistura do Selector de Cores pode ser usada como uma ferramenta "
"para a mistura de cores fora da área de desenho."

#: ../../reference_manual/tools/color_selector.rst:32
msgid ""
"At the very bottom is the Info Box, which displays per-channel data about "
"your most recently picked color. Color data can be shown as 8-bit numbers or "
"percentages."
msgstr ""
"Mesmo no fundo existe a Área Informativa, que mostra os dados por cada canal "
"da sua cor escolhida mais recentemente. Os dados das cores podem ser "
"apresentados como números de 8-bits ou como percentagens."
