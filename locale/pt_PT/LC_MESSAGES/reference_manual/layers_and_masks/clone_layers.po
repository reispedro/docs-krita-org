# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-08 17:39+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Krita\n"

#: ../../reference_manual/layers_and_masks/clone_layers.rst:1
msgid "How to use clone layers."
msgstr "Como usar as camadas de clonagem."

#: ../../reference_manual/layers_and_masks/clone_layers.rst:15
msgid "Layers"
msgstr "Camadas"

#: ../../reference_manual/layers_and_masks/clone_layers.rst:15
msgid "Linked Clone"
msgstr "Clone Ligado"

#: ../../reference_manual/layers_and_masks/clone_layers.rst:15
msgid "Clone Layer"
msgstr "Clonar a Camada"

#: ../../reference_manual/layers_and_masks/clone_layers.rst:20
msgid "Clone Layers"
msgstr "Camadas de Clonagem"

#: ../../reference_manual/layers_and_masks/clone_layers.rst:22
msgid ""
"A clone layer is a layer that keeps an up-to-date copy of another layer. You "
"cannot draw or paint on it directly, but it can be used to create effects by "
"applying different types of layers and masks (e.g. filter layers or masks)."
msgstr ""
"Uma camada de clonagem é uma camada que mantém uma cópia actualizada de "
"outra camada. Não poderá desenhar ou pintar sobre ela directamente, mas pode "
"ser usada para criar efeitos, aplicando diferentes tipos de camadas e "
"máscaras (p.ex., camadas ou máscaras de filtragem)."

#: ../../reference_manual/layers_and_masks/clone_layers.rst:25
msgid "Example uses of Clone Layers."
msgstr "Usos de exemplo das Camadas de Clonagem."

#: ../../reference_manual/layers_and_masks/clone_layers.rst:27
msgid ""
"For example, if you were painting a picture of some magic person and wanted "
"to create a glow around them that was updated as you updated your character, "
"you could:"
msgstr ""
"Por exemplo, se estiver a pintar uma imagem de alguma pessoa mágica e quiser "
"criar um brilho à volta dela que fosse alterado à medida que actualizasse a "
"sua personagem, seguem-se alguns passos possíveis:"

#: ../../reference_manual/layers_and_masks/clone_layers.rst:29
msgid "Have a Paint Layer where you draw your character"
msgstr "Ter uma Camada de Pintura onde poderá desenhar a sua personagem"

#: ../../reference_manual/layers_and_masks/clone_layers.rst:30
msgid ""
"Use the Clone Layer feature to create a clone of the layer that you drew "
"your character on"
msgstr ""
"Use a funcionalidade da Camada de Pintura para criar um clone da camada "
"sobre a qual desenhou o seu personagem"

#: ../../reference_manual/layers_and_masks/clone_layers.rst:31
msgid ""
"Apply an HSV filter mask to the clone layer to make the shapes on it white "
"(or blue, or green etc.)"
msgstr ""
"Aplique uma máscara de filtragem HSV à camada de clonagem para tornar as "
"formas nela brancas (ou azuis, ou verdes, etc.)"

#: ../../reference_manual/layers_and_masks/clone_layers.rst:32
msgid "Apply a blur filter mask to the clone layer so it looks like a \"glow\""
msgstr ""
"Aplique um filtro de borrão à camada de clonagem, de forma a parecer que tem "
"um \"brilho\""

#: ../../reference_manual/layers_and_masks/clone_layers.rst:34
msgid ""
"As you keep painting and adding details, erasing on the first layer, Krita "
"will automatically update the clone layer, making your \"glow\" apply to "
"every change you make."
msgstr ""
"À medida que continua a pintar e a adicionar detalhes, a apagar na primeira "
"camada, o Krita irá actualizar automaticamente a camada de clonagem, fazendo "
"com que o seu \"brilho\" se aplique a todas as alterações que fizer."
