# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-17 18:01+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Krita QML\n"

#: ../../reference_manual/dockers/touch_docker.rst:1
msgid "Overview of the touch docker."
msgstr "Introdução à área táctil."

#: ../../reference_manual/dockers/touch_docker.rst:10
msgid "Touch"
msgstr "Toque"

#: ../../reference_manual/dockers/touch_docker.rst:10
msgid "Finger"
msgstr "Dedo"

#: ../../reference_manual/dockers/touch_docker.rst:10
msgid "Tablet UI"
msgstr "UI da Tablete"

#: ../../reference_manual/dockers/touch_docker.rst:15
msgid "Touch Docker"
msgstr "Área Acoplável Táctil"

#: ../../reference_manual/dockers/touch_docker.rst:17
msgid ""
"The Touch Docker is a QML docker with several convenient actions on it. Its "
"purpose is to aid those who use Krita on a touch-enabled screen by providing "
"bigger gui elements."
msgstr ""
"A Área Táctil é uma área acoplável em QML com diversas acções úteis nela. O "
"seu objectivo é ajudar os que usam o Krita num ecrã táctil, oferecendo "
"elementos gráficos maiores."

#: ../../reference_manual/dockers/touch_docker.rst:19
msgid "Its actions are..."
msgstr "As suas acções são..."

#: ../../reference_manual/dockers/touch_docker.rst:22
msgid "Open File"
msgstr "Abrir um Ficheiro"

#: ../../reference_manual/dockers/touch_docker.rst:22
msgid "Save File"
msgstr "Gravar o Ficheiro"

#: ../../reference_manual/dockers/touch_docker.rst:22
msgid "Save As"
msgstr "Gravar Como"

#: ../../reference_manual/dockers/touch_docker.rst:24
msgid "Undo"
msgstr "Desfazer"

#: ../../reference_manual/dockers/touch_docker.rst:24
msgid "Redo"
msgstr "Refazer"

#: ../../reference_manual/dockers/touch_docker.rst:26
msgid "Decrease Opacity"
msgstr "Reduzir a Opacidade"

#: ../../reference_manual/dockers/touch_docker.rst:28
msgid "Increase Opacity"
msgstr "Aumentar a Opacidade"

#: ../../reference_manual/dockers/touch_docker.rst:30
msgid "Increase Lightness"
msgstr "Aumentar a Luminosidade"

#: ../../reference_manual/dockers/touch_docker.rst:32
msgid "Decrease Lightness"
msgstr "Diminuir a Luminosidade"

#: ../../reference_manual/dockers/touch_docker.rst:34
msgid "Zoom in"
msgstr "Ampliar"

#: ../../reference_manual/dockers/touch_docker.rst:36
msgid "Rotate Counter Clockwise 15°"
msgstr "Rodar 15° no Sentido Anti-Horário"

#: ../../reference_manual/dockers/touch_docker.rst:36
msgid "Reset Canvas Rotation"
msgstr "Limpar a Rotação da Área de Desenho"

#: ../../reference_manual/dockers/touch_docker.rst:36
msgid "Rotate Clockwise 15°"
msgstr "Rodar 15° no Sentido Horário"

#: ../../reference_manual/dockers/touch_docker.rst:38
msgid "Zoom out"
msgstr "Reduzir"

#: ../../reference_manual/dockers/touch_docker.rst:40
msgid "Decrease Brush Size"
msgstr "Diminuir o Tamanho do Pincel"

#: ../../reference_manual/dockers/touch_docker.rst:42
msgid "Increase Brush Size"
msgstr "Aumentar o Tamanho do Pincel"

#: ../../reference_manual/dockers/touch_docker.rst:44
msgid "Delete Layer Contents"
msgstr "Apagar o Conteúdo da Camada"
