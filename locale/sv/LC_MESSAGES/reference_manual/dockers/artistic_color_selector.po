# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-08-25 16:54+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: vänsterknapp"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: högerknapp"

#: ../../reference_manual/dockers/artistic_color_selector.rst:1
msgid "Overview of the artistic color selector docker."
msgstr "Översikt av den konstnärlig färgväljarpanelen"

#: ../../reference_manual/dockers/artistic_color_selector.rst:12
msgid "Color"
msgstr "Färg"

#: ../../reference_manual/dockers/artistic_color_selector.rst:12
msgid "Color Selector"
msgstr "Färgväljare"

#: ../../reference_manual/dockers/artistic_color_selector.rst:12
msgid "Artistic Color Selector"
msgstr "Konstnärlig färgväljare"

#: ../../reference_manual/dockers/artistic_color_selector.rst:17
msgid "Artistic Color Selector Docker"
msgstr "Konstnärlig färgväljarpanel"

#: ../../reference_manual/dockers/artistic_color_selector.rst:19
msgid "A color selector inspired by traditional color wheel and workflows."
msgstr "En färgväljare inspirerad av traditionella färghjul och arbetsflöden."

#: ../../reference_manual/dockers/artistic_color_selector.rst:22
msgid "Usage"
msgstr "Användning"

#: ../../reference_manual/dockers/artistic_color_selector.rst:26
msgid ".. image:: images/dockers/Krita_Artistic_Color_Selector_Docker.png"
msgstr ".. image:: images/dockers/Krita_Artistic_Color_Selector_Docker.png"

#: ../../reference_manual/dockers/artistic_color_selector.rst:26
msgid "Artistic color selector with a gamut mask."
msgstr "Konstnärlig färgväljare med en färgomfångsmask."

#: ../../reference_manual/dockers/artistic_color_selector.rst:28
msgid ""
"Select hue and saturation on the wheel (5) and value on the value scale (4). "
"|mouseleft| changes foreground color (6). |mouseright| changes background "
"color (7)."
msgstr ""
"Välj färgton och färgmättnad med hjulet (5) och värde med värdeskalan (4). "
"Vänster musknapp ändrar förgrundsfärg (6). Höger musknapp ändrar "
"bakgrundsfärg (7)."

#: ../../reference_manual/dockers/artistic_color_selector.rst:30
msgid ""
"The blip shows the position of current foreground color on the wheel "
"(black&white circle) and on the value scale (black&white line). Last "
"selected swatches are outlined."
msgstr ""
"Punkten visar positionen för nuvarande förgrundsfärg på hjulet (svartvit "
"cirkel) och på värdeskalan (svartvit linje). Senaste markerade färgrutor "
"visas med konturer."

#: ../../reference_manual/dockers/artistic_color_selector.rst:32
msgid ""
"Parameters of the wheel can be set in :ref:"
"`artistic_color_selector_docker_wheel_preferences` menu (2). Selector "
"settings are found under :ref:"
"`artistic_color_selector_docker_selector_settings` menu (3)."
msgstr ""
"Hjulets parametrar kan ställas in med menyn :ref:"
"`artistic_color_selector_docker_wheel_preferences` (2). Väljarinställningar "
"finns i menyn :ref:`artistic_color_selector_docker_selector_settings` (3)."

#: ../../reference_manual/dockers/artistic_color_selector.rst:35
msgid "Gamut Masking"
msgstr "Färgomfångsmaskering"

#: ../../reference_manual/dockers/artistic_color_selector.rst:37
msgid ""
"You can select and manage your gamut masks in the :ref:`gamut_mask_docker`."
msgstr ""
"Man kan välja och hantera färgomfångsmasker med :ref:`gamut_mask_docker`."

#: ../../reference_manual/dockers/artistic_color_selector.rst:39
msgid ""
"In the gamut masking toolbar (1) you can toggle the selected mask off and on "
"(left button). You can also rotate the mask with the rotation slider (right)."
msgstr ""
"I verktygsraden för färgomfångsmaskering (1) kan man stänga av och sätta på "
"den valda masken (knappen till vänster). Man kan också rotera masken med "
"rotationsreglaget (till höger)."

#: ../../reference_manual/dockers/artistic_color_selector.rst:45
msgid "Color wheel preferences"
msgstr "Färghjulsinställningar"

#: ../../reference_manual/dockers/artistic_color_selector.rst:50
msgid ".. image:: images/dockers/Krita_Artistic_Color_Selector_Docker_3.png"
msgstr ".. image:: images/dockers/Krita_Artistic_Color_Selector_Docker_3.png"

#: ../../reference_manual/dockers/artistic_color_selector.rst:50
msgid "Color wheel preferences."
msgstr "Färghjulsinställningar."

#: ../../reference_manual/dockers/artistic_color_selector.rst:53
msgid "Sliders 1, 2, and 3"
msgstr "Skjutreglage 1, 2 och 3"

#: ../../reference_manual/dockers/artistic_color_selector.rst:53
msgid ""
"Adjust the number of steps of the value scale, number of hue sectors and "
"saturation rings on the wheel, respectively."
msgstr ""
"Justera värdeskalans antal steg, antal färgtonssektorer och "
"färgmättnadsringar på hjulet."

#: ../../reference_manual/dockers/artistic_color_selector.rst:56
msgid "Continuous Mode"
msgstr "Kontinuerligt läge"

#: ../../reference_manual/dockers/artistic_color_selector.rst:56
msgid ""
"The value scale and hue sectors can also be set to continuous mode (with the "
"infinity icon on the right of the slider). If toggled on, the respective "
"area shows a continuous gradient instead of the discrete swatches."
msgstr ""
"Värdeskalan och färgtonssektorer kan också ställas in till kontinuerligt "
"läge (med oändlighetsikonen till höger om skjutreglaget). Om den sätts på "
"visar respektive område en kontinuerlig toning istället för diskreta "
"färgrutor."

#: ../../reference_manual/dockers/artistic_color_selector.rst:59
msgid "Invert saturation (4)"
msgstr "Invertera färgmättnad (4)"

#: ../../reference_manual/dockers/artistic_color_selector.rst:59
msgid ""
"Changes the order of saturation rings within the the hue sectors. By "
"default, the wheel has gray in the center and most saturated colors on the "
"perimeter. :guilabel:`Invert saturation` puts gray on the perimeter and most "
"saturated colors in the center."
msgstr ""
"Ändrar färgmättnadsringarnas ordningen inom färgtonssektorn. Normalt har "
"hjulet grått i mitten och de mest mättade färgerna längs omkretsen. :"
"guilabel:`Invertera färgmättnad` placerar grått längs omkretsen och de mest "
"mättade färgerna i mitten."

#: ../../reference_manual/dockers/artistic_color_selector.rst:62
msgid ""
"Loads default values for the sliders 1, 2, and 3. These default values are "
"configured in selector settings."
msgstr ""
"Läser in förvalda värden för skjutreglagen 1, 2 och 3. De förvalda värdena "
"ställs in i väljarinställningarna."

#: ../../reference_manual/dockers/artistic_color_selector.rst:63
msgid "Reset to default (5)"
msgstr "Återställ till förval (5)"

#: ../../reference_manual/dockers/artistic_color_selector.rst:68
msgid "Selector settings"
msgstr "Väljarinställningar"

#: ../../reference_manual/dockers/artistic_color_selector.rst:72
msgid ".. image:: images/dockers/Krita_Artistic_Color_Selector_Docker_2.png"
msgstr ".. image:: images/dockers/Krita_Artistic_Color_Selector_Docker_2.png"

#: ../../reference_manual/dockers/artistic_color_selector.rst:72
msgid "Selector settings menu."
msgstr "Väljarinställningsmeny."

#: ../../reference_manual/dockers/artistic_color_selector.rst:75
msgid "Show background color indicator"
msgstr "Visa bakgrundsfärgindikering"

#: ../../reference_manual/dockers/artistic_color_selector.rst:76
msgid "Toggles the bottom-right triangle with current background color."
msgstr "Ändrar triangeln längst ner till höger till nuvarande bakgrundsfärg."

#: ../../reference_manual/dockers/artistic_color_selector.rst:78
msgid "Selector Appearance (1)"
msgstr "Väljarutseende (1)"

#: ../../reference_manual/dockers/artistic_color_selector.rst:78
msgid "Show numbered value scale"
msgstr "Visa numrerad värdeskala"

#: ../../reference_manual/dockers/artistic_color_selector.rst:78
msgid ""
"If checked, the value scale includes a comparative gray scale with lightness "
"percentage."
msgstr ""
"Om markerad, inkluderar värdeskalan en jämförande gråskala med "
"ljushetsprocent."

#: ../../reference_manual/dockers/artistic_color_selector.rst:81
msgid ""
"Set the color model used by the selector. For detailed information on color "
"models, see :ref:`color_models`."
msgstr ""
"Ställ in färgmodellen använd av väljaren. För detaljerad information om "
"färgmodeller, se :ref:`color_models`."

#: ../../reference_manual/dockers/artistic_color_selector.rst:84
msgid "Color Space (2)"
msgstr "Färgrymd (2)"

#: ../../reference_manual/dockers/artistic_color_selector.rst:84
msgid "Luma Coefficients (3)"
msgstr "Luminanskoefficienter (3)"

#: ../../reference_manual/dockers/artistic_color_selector.rst:84
msgid ""
"If the selector's color space is HSY, you can set custom Luma coefficients "
"and the amount of gamma correction applied to the value scale (set to 1.0 "
"for linear scale; see :ref:`linear_and_gamma`)."
msgstr ""
"Om väljarens färgrymd är HSY, kan man ställa in egna luminanskoefficienter "
"och hur stor gammakorrigering som används för värdeskalan (ställ in 1,0 för "
"linjär skala, se :ref:`linear_and_gamma`)."

#: ../../reference_manual/dockers/artistic_color_selector.rst:87
msgid "Gamut Masking Behavior (4)"
msgstr "Beteende vid färgomfångsmaskering (4)"

#: ../../reference_manual/dockers/artistic_color_selector.rst:87
msgid ""
"The selector can be set either to :guilabel:`Enforce gamut mask`, so that "
"colors outside the mask cannot be selected, or to :guilabel:`Just show the "
"shapes`, where the mask is visible but color selection is not limited."
msgstr ""
"Väljaren kan antingen ställas in till :guilabel:`Tvinga färgomfångsmask`, så "
"att färger utanför masken inte kan väljas, eller till :guilabel:`Visa bara "
"formerna`, där masken är synlig men färgvalet inte är begränsat."

#: ../../reference_manual/dockers/artistic_color_selector.rst:90
msgid "Default Selector Steps Settings"
msgstr "Standardinställningar av väljarsteg"

#: ../../reference_manual/dockers/artistic_color_selector.rst:90
msgid ""
"Values the color wheel and value scale will be reset to default when the :"
"guilabel:`Reset to default` button in :ref:"
"`artistic_color_selector_docker_wheel_preferences` is pressed."
msgstr ""
"Värdena i färghjulet och värdeskalan återställs till förval när knappen :"
"guilabel:`Återställ till förval` i :ref:"
"`artistic_color_selector_docker_wheel_preferences` klickas."

#: ../../reference_manual/dockers/artistic_color_selector.rst:93
msgid "External Info"
msgstr "Extern information"

#: ../../reference_manual/dockers/artistic_color_selector.rst:94
msgid ""
"`HSI and HSY for Krita’s advanced colour selector by Wolthera van Hövell tot "
"Westerflier. <https://wolthera.info/?p=726>`_"
msgstr ""
"`HSI and HSY for Krita’s advanced colour selector av Wolthera van Hövell tot "
"Westerflier. <https://wolthera.info/?p=726>`_"

#: ../../reference_manual/dockers/artistic_color_selector.rst:95
msgid ""
"`The Color Wheel, Part 7 by James Gurney. <https://gurneyjourney.blogspot."
"com/2010/02/color-wheel-part-7.html>`_"
msgstr ""
"`Färghjulet, Del 7 av James Gurney. <https://gurneyjourney.blogspot."
"com/2020/01/color-wheel-masking-part-7.html>`_"

#~ msgid "Preference"
#~ msgstr "Preferens"

#~ msgid "Reset"
#~ msgstr "Återställ"

#~ msgid "Absolute"
#~ msgstr "Absolut"
