# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-11 10:54+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.1\n"

#: ../../reference_manual/dockers/small_color_selector.rst:1
msgid "Overview of the small color selector docker."
msgstr "Overzicht van de vastzetter De kleine kleurenkiezer."

#: ../../reference_manual/dockers/small_color_selector.rst:11
#: ../../reference_manual/dockers/small_color_selector.rst:16
msgid "Small Color Selector"
msgstr "Kleine kleurenkiezer"

#: ../../reference_manual/dockers/small_color_selector.rst:11
msgid "Color"
msgstr "Kleur"

#: ../../reference_manual/dockers/small_color_selector.rst:11
msgid "Color Selector"
msgstr "Kleurenkiezer"

#: ../../reference_manual/dockers/small_color_selector.rst:19
msgid ".. image:: images/dockers/Krita_Small_Color_Selector_Docker.png"
msgstr ".. image:: images/dockers/Krita_Small_Color_Selector_Docker.png"

#: ../../reference_manual/dockers/small_color_selector.rst:20
msgid ""
"This is Krita's most simple color selector. On the left there's a bar with "
"the hue, and on the right a square where you can pick the value and "
"saturation."
msgstr ""
"Dit is de meest eenvoudige kleurenkiezer van Krita. Links is er een balk met "
"de tint en rechts een vierkant waar u de waarde en verzadiging kunt kiezen."

#: ../../reference_manual/dockers/small_color_selector.rst:24
msgid ""
"The small color selector is the only selector which can show HDR values. "
"When your build of Krita is HDR enabled and you are on Windows, you can drag "
"the slider at the bottom to increase the 'nits' of the colors in the small "
"selector. This is the direct value of the brightness of the colors, and you "
"need a value above 100 (100 being the maximum value used for the brightest "
"value of sRGB colors), to have an HDR color. The small color selector will "
"also select wide gamut values."
msgstr ""
"De kleine kleurenkiezer is de enige kiezer die HDR waarden kan tonen. "
"Wanneer uw gebouwde Krita HDR heeft ingeschakeld en u bent op Windows, dan "
"kunt u de schuifregelaar onderaan verslepen om de 'nits' van de kleuren in "
"de kleine kiezer vergroten. Dit is de directe waarde van de helderheid van "
"de kleuren en u heeft een waarde boven 100 nodig (100 is de maximum waarde "
"gebruikt voor de helderste waarde van sRGB kleuren), om een HDR kleur te "
"hebben. De kleine kleurenkiezer zal ook brede gamut-waarden selecteren."
