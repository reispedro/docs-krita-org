msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_general_concepts___file_formats___file_pbgpm.pot\n"

#: ../../<generated>:1
msgid ".ppm"
msgstr ".ppm"

#: ../../general_concepts/file_formats/file_pbgpm.rst:1
msgid "The PBM, PGM and PPM file formats as exported by Krita."
msgstr ""

#: ../../general_concepts/file_formats/file_pbgpm.rst:10
msgid "*.pbm"
msgstr ""

#: ../../general_concepts/file_formats/file_pbgpm.rst:10
msgid "*.pgm"
msgstr ""

#: ../../general_concepts/file_formats/file_pbgpm.rst:10
msgid "*.ppm"
msgstr ""

#: ../../general_concepts/file_formats/file_pbgpm.rst:10
msgid "PBM"
msgstr ""

#: ../../general_concepts/file_formats/file_pbgpm.rst:10
msgid "PGM"
msgstr ""

#: ../../general_concepts/file_formats/file_pbgpm.rst:10
msgid "PPM"
msgstr ""

#: ../../general_concepts/file_formats/file_pbgpm.rst:17
msgid "\\*.pbm, \\*.pgm and \\*.ppm"
msgstr ""

#: ../../general_concepts/file_formats/file_pbgpm.rst:18
msgid ""
"``.pbm``, ``.pgm`` and ``.ppm`` are a series of file-formats with a similar "
"logic to them. They are designed to save images in a way that the result can "
"be read as an ASCII file, from back when email clients couldn't read images "
"reliably."
msgstr ""

#: ../../general_concepts/file_formats/file_pbgpm.rst:20
msgid ""
"They are very old file formats, and not used outside of very specialized "
"usecases, such as embedding images inside code."
msgstr ""
"它们是年代久远的文件格式，只在非常特定的条件下使用，例如把图像嵌入到代码里"
"面。"

#: ../../general_concepts/file_formats/file_pbgpm.rst:22
msgid ".pbm"
msgstr ".pbm"

#: ../../general_concepts/file_formats/file_pbgpm.rst:23
msgid "One-bit and can only show strict black and white."
msgstr ""

#: ../../general_concepts/file_formats/file_pbgpm.rst:24
msgid ".pgm"
msgstr ".pgm"

#: ../../general_concepts/file_formats/file_pbgpm.rst:25
msgid "Can show 255 values of gray (8bit)."
msgstr ""

#: ../../general_concepts/file_formats/file_pbgpm.rst:27
msgid "Can show 8bit rgb values."
msgstr ""
