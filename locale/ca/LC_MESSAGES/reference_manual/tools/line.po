# Translation of docs_krita_org_reference_manual___tools___line.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-24 16:21+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../<generated>:1
msgid "Preview"
msgstr "Vista prèvia"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: clic esquerre del ratolí"

#: ../../<rst_epilog>:24
msgid ""
".. image:: images/icons/line_tool.svg\n"
"   :alt: toolline"
msgstr ""
".. image:: images/icons/line_tool.svg\n"
"   :alt: eina de línia"

#: ../../reference_manual/tools/line.rst:1
msgid "Krita's line tool reference."
msgstr "Referència de l'eina Línia del Krita."

#: ../../reference_manual/tools/line.rst:11
msgid "Tools"
msgstr "Eines"

#: ../../reference_manual/tools/line.rst:11
msgid "Line"
msgstr "Línia"

#: ../../reference_manual/tools/line.rst:11
msgid "Straight Line"
msgstr "Línia recta"

#: ../../reference_manual/tools/line.rst:16
msgid "Straight Line Tool"
msgstr "Eina de línia recta"

#: ../../reference_manual/tools/line.rst:18
msgid "|toolline|"
msgstr "|toolline|"

#: ../../reference_manual/tools/line.rst:21
msgid ""
"This tool is used to draw lines. Click the |mouseleft| to indicate the first "
"endpoint, keep the button pressed, drag to the second endpoint and release "
"the button."
msgstr ""
"Aquesta eina s'utilitza per a dibuixar línies. Feu |mouseleft| per indicar "
"el primer punt final, manteniu premut el botó, arrossegueu fins al segon "
"punt final i deixeu anar el botó."

#: ../../reference_manual/tools/line.rst:24
msgid "Hotkeys and Sticky Keys"
msgstr "Dreceres i tecles apegaloses"

#: ../../reference_manual/tools/line.rst:26
msgid ""
"To activate the Line tool from freehand brush mode, use the :kbd:`V` key. "
"Use other keys afterwards to constraint the line."
msgstr ""
"Per activar l'eina de Línia des del mode pinzell a mà alçada, utilitzeu la "
"tecla :kbd:`V`. Utilitzeu després altres tecles per a restringir la línia."

#: ../../reference_manual/tools/line.rst:28
msgid ""
"Use the :kbd:`Shift` key while holding the mouse button to constrain the "
"angle to multiples of 15º. You can press the :kbd:`Alt` key while still "
"keeping the |mouseleft| down to move the line to a different location."
msgstr ""
"Utilitzeu la tecla :kbd:`Majús.` mentre manteniu premut el botó del ratolí "
"per a restringir l'angle a múltiples de 15º. Podeu prémer la tecla :kbd:"
"`Alt` mentre premeu el |mouseleft| per a moure la línia a una ubicació "
"diferent."

#: ../../reference_manual/tools/line.rst:32
msgid ""
"Using the :kbd:`Shift` keys BEFORE pushing the holding the left mouse button/"
"stylus down will, in default Krita, activate the quick brush-resize. Be sure "
"to use the :kbd:`Shift` key after."
msgstr ""
"Utilitzeu la tecla :kbd:`Majús.` ABANS de prémer el botó esquerre del ratolí/"
"prémer amb el llapis, de manera predeterminada en el Krita, activarà el "
"canvi de mida ràpid del pinzell. Assegureu-vos d'utilitzar després la tecla :"
"kbd:`Majús.`."

#: ../../reference_manual/tools/line.rst:35
msgid "Tool Options"
msgstr "Opcions de l'eina"

#: ../../reference_manual/tools/line.rst:37
msgid ""
"The following options allow you to modify the end-look of the straight-line "
"stroke with tablet-values. Of course, this only work for tablets, and "
"currently only on Paint Layers."
msgstr ""
"Les següents opcions permeten modificar l'aspecte final del traç de la línia "
"recta amb els valors de la tauleta. Per descomptat, això només funcionarà "
"per a les tauletes, i actualment només sobre les Capes de pintura."

#: ../../reference_manual/tools/line.rst:40
msgid "Use sensors"
msgstr "Usa els sensors"

#: ../../reference_manual/tools/line.rst:41
msgid ""
"This will draw the line while taking sensors into account. To use this "
"effectively, start the line and trace the path like you would when drawing a "
"straight line before releasing. If you make a mistake, make the line shorter "
"and start over."
msgstr ""
"Dibuixarà la línia tenint en compte els sensors. Per utilitzar-ho de manera "
"efectiva, comenceu la línia i traceu el camí com ho faríeu en dibuixar una "
"línia recta abans de deixar anar. Si cometeu un error, escurceu la línia i "
"comenceu de nou."

#: ../../reference_manual/tools/line.rst:43
msgid ""
"This will show the old-fashioned preview line so you know where your line "
"will end up."
msgstr ""
"Mostrarà la vista prèvia de la línia antiga perquè sapigueu on acabarà la "
"vostra línia."
