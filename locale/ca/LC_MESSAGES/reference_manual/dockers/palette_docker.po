# Translation of docs_krita_org_reference_manual___dockers___palette_docker.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-09 03:41+0200\n"
"PO-Revision-Date: 2019-08-24 16:40+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: clic esquerre del ratolí"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: clic dret del ratolí"

#: ../../reference_manual/dockers/palette_docker.rst:1
msgid "Overview of the palette docker."
msgstr "Vista general de l'acoblador Paleta."

#: ../../reference_manual/dockers/palette_docker.rst:12
#: ../../reference_manual/dockers/palette_docker.rst:74
msgid "Color"
msgstr "Color"

#: ../../reference_manual/dockers/palette_docker.rst:12
msgid "Palettes"
msgstr "Paletes"

#: ../../reference_manual/dockers/palette_docker.rst:12
msgid "Swatches"
msgstr "Mostres"

#: ../../reference_manual/dockers/palette_docker.rst:17
msgid "Palette Docker"
msgstr "Acoblador Paleta"

#: ../../reference_manual/dockers/palette_docker.rst:19
msgid ""
"The palette docker displays various color swatches for quick use. It also "
"supports editing palettes and organizing colors into groups, as well as "
"arbitrary positioning of swatches."
msgstr ""
"L'acoblador Paleta mostra diverses mostres de color per a un ús ràpid. També "
"admet l'edició de les paletes i l'organització dels colors en grups, així "
"com la col·locació arbitrària de les mostres."

#: ../../reference_manual/dockers/palette_docker.rst:23
msgid ""
"The palette docker was overhauled in 4.2, allowing for grid ordering, "
"storing palette in the document and more."
msgstr ""
"L'acoblador Paleta es va revisar a la versió 4.2, el que ha permés "
"l'ordenament de la quadrícula, l'emmagatzematge de la paleta en el document "
"i més."

#: ../../reference_manual/dockers/palette_docker.rst:26
msgid ".. image:: images/dockers/Palette-docker.png"
msgstr ".. image:: images/dockers/Palette-docker.png"

#: ../../reference_manual/dockers/palette_docker.rst:27
msgid ""
"You can choose from various default palettes or you can add your own colors "
"to the palette."
msgstr ""
"Podeu escollir entre diverses paletes predeterminades o podeu afegir els "
"vostres propis colors a la paleta."

#: ../../reference_manual/dockers/palette_docker.rst:29
msgid ""
"To choose from the default palettes click on the icon in the bottom left "
"corner of the docker, it will show a list of pre-loaded color palettes. You "
"can click on one and to load it into the docker, or click on import "
"resources to load your own color palette from a file. Creating a new palette "
"can be done by pressing the :guilabel:`+`. Fill out the :guilabel:`name` "
"input, pressing :guilabel:`Save` and Krita will select your new palette for "
"you."
msgstr ""
"Per a triar entre les paletes predeterminades, feu clic sobre la icona de la "
"cantonada inferior esquerra de l'acoblador, això mostrarà una llista de "
"paletes de colors precarregades. Podreu fer clic en una i carregar-la a "
"l'acoblador, o fer clic sobre els recursos importats per a carregar la "
"vostra pròpia paleta de colors des d'un fitxer. Es pot crear una paleta nova "
"prement :guilabel:`+`. Empleneu l'entrada :guilabel:`Nom`, premeu :guilabel:"
"`Desa` i el Krita seleccionarà la paleta nova."

#: ../../reference_manual/dockers/palette_docker.rst:32
msgid ""
"Since 4.2 Krita's color palettes are not just a list of colors to store, but "
"also a grid to organize them on. That's why you will get a grid with "
"'transparency checkers', indicating that there is no entry. To add an entry, "
"just click a swatch and a new entry will be added with a default name and "
"the current foreground color."
msgstr ""
"Des de la versió 4.2, les paletes de colors del Krita no són només una "
"llista de colors per emmagatzemar, sinó també una quadrícula per organitzar-"
"los. És per això que obtindreu una quadrícula amb «marcadors de "
"transparència», els quals indicaran que no hi ha cap entrada. Per afegir una "
"entrada, simplement feu clic a una mostra i s'afegirà una entrada nova amb "
"un nom predeterminat i el color de primer pla actual."

#: ../../reference_manual/dockers/palette_docker.rst:34
msgid "Selecting colors is done by |mouseleft| on a swatch."
msgstr "La selecció de colors es realitza fent |mouseleft| sobre una mostra."

#: ../../reference_manual/dockers/palette_docker.rst:35
msgid ""
"Pressing the delete icon will remove the selected swatch or group. When "
"removing a group, Krita will always ask whether you'd like to keep the "
"swatches. If so, the group will be merged with the default group."
msgstr ""
"En prémer la icona suprimeix s'eliminarà la mostra o el grup seleccionat. En "
"eliminar un grup, el Krita sempre us demanarà si voleu conservar les "
"mostres. Si és així, el grup es fusionarà amb el grup predeterminat."

#: ../../reference_manual/dockers/palette_docker.rst:36
msgid ""
"Double |mouseleft| a swatch will call up the edit window where you can "
"change the color, the name, the id and whether it's a spot color. On a group "
"this will allow you to set the group name."
msgstr ""
"Fer doble |mouseleft| sobre una mostra obrirà la finestra d'edició des d'on "
"podreu canviar el color, el nom, l'identificador i si és un color directe. "
"En un grup, això permetrà establir el nom del grup."

#: ../../reference_manual/dockers/palette_docker.rst:37
msgid ""
"|mouseleft| drag will allow you to drag and drop swatches and groups to "
"order them."
msgstr ""
"Fer |mouseleft| i arrossegar permetrà arrossegar i deixar anar les mostres i "
"grups per ordenar-los."

#: ../../reference_manual/dockers/palette_docker.rst:38
msgid ""
"|mouseright| on a swatch will give you a context menu with modify and delete "
"options."
msgstr ""
"Fer |mouseright| sobre una mostra donarà un menú contextual amb les opcions "
"modifica i suprimeix."

#: ../../reference_manual/dockers/palette_docker.rst:39
msgid "Pressing the :guilabel:`+` icon will allow you to add a new swatch."
msgstr "En prémer la icona :guilabel:`+` es podrà afegir una mostra nova."

#: ../../reference_manual/dockers/palette_docker.rst:40
msgid ""
"The drop down contains all the entries, id numbers and names. When a color "
"is a spot color the thumbnail is circular. You can use the dropdown to "
"search on color name or id."
msgstr ""
"La llista desplegable conté totes les entrades, números d'identificació i "
"noms. Quan un color és un color directe, la miniatura serà circular. Podreu "
"utilitzar la llista desplegable per a cercar el nom o l'identificador del "
"color."

#: ../../reference_manual/dockers/palette_docker.rst:43
msgid ""
"Pressing the Folder icon will allow you to modify the palette. Here you can "
"add more columns, modify the default group's rows, or add more groups and "
"modify their rows."
msgstr ""
"En prémer la icona de Carpeta podreu modificar la paleta. Aquí podreu afegir "
"més columnes, modificar les files del grup predeterminat o afegir més grups "
"i modificar les seves files."

#: ../../reference_manual/dockers/palette_docker.rst:45
msgid "Palette Name"
msgstr "Nom de la paleta"

#: ../../reference_manual/dockers/palette_docker.rst:46
msgid ""
"Modify the palette name. This is the proper name for the palette as shown in "
"the palette chooser dropdown."
msgstr ""
"Modifica el nom de la paleta. Aquest serà el nom propi de la paleta, tal com "
"es mostrarà a la llista desplegable del selector de paletes."

#: ../../reference_manual/dockers/palette_docker.rst:47
msgid "File name"
msgstr "Nom del fitxer"

#: ../../reference_manual/dockers/palette_docker.rst:48
msgid ""
"This is the file name of the palette, which should be file system friendly. "
"(Avoid quotation marks, for example)."
msgstr ""
"Aquest serà el nom del fitxer de la paleta, el qual haurà de ser amigable "
"amb el sistema de fitxers. (Eviteu les cometes, per exemple)."

#: ../../reference_manual/dockers/palette_docker.rst:49
msgid "Column Count"
msgstr "Nombre de columnes"

#: ../../reference_manual/dockers/palette_docker.rst:50
msgid ""
"The amount of columns in this palette. This counts for all entries. If you "
"accidentally make it smaller than the amount of entries that take up "
"columns, you can still make it bigger until the next restart of Krita."
msgstr ""
"La quantitat de columnes en aquesta paleta. Això comptarà totes les "
"entrades. Si accidentalment la feu més petita que la quantitat d'entrades "
"que ocupen les columnes, encara podreu fer-la més gran fins al següent "
"reinici del Krita."

#: ../../reference_manual/dockers/palette_docker.rst:52
msgid "Whether to store said palette in the document or resource folder."
msgstr ""
"Ja sigui per emmagatzemar aquesta paleta en el document o a la carpeta de "
"recursos."

#: ../../reference_manual/dockers/palette_docker.rst:54
msgid "Resource Folder"
msgstr "Carpeta de recursos"

#: ../../reference_manual/dockers/palette_docker.rst:55
msgid "The default, the palette will be stored in the resource folder."
msgstr ""
"De manera predeterminada, la paleta s'emmagatzemarà a la carpeta de recursos."

#: ../../reference_manual/dockers/palette_docker.rst:57
msgid "Where is the palette stored:"
msgstr "On s'emmagatzema la paleta:"

#: ../../reference_manual/dockers/palette_docker.rst:57
msgid "Document"
msgstr "Document"

#: ../../reference_manual/dockers/palette_docker.rst:57
msgid ""
"The palette will be removed from the resource folder and stored in the "
"document upon save. It will be loaded into the resources upon loading the "
"document."
msgstr ""
"Quan es desi la paleta, s'eliminarà de la carpeta de recursos i "
"s'emmagatzemarà en el document. Es carregarà en els recursos en carregar el "
"document."

#: ../../reference_manual/dockers/palette_docker.rst:59
msgid "Add group"
msgstr "Afegeix un grup"

#: ../../reference_manual/dockers/palette_docker.rst:60
msgid ""
"Add a new group. On clicking you will be asked for a name and a set of rows."
msgstr ""
"Afegirà un grup nou. En fer clic se us demanarà un nom i un conjunt de files."

#: ../../reference_manual/dockers/palette_docker.rst:62
msgid ""
"Here you can configure the groups. The dropdown has a selection of groups. "
"The default group is at top."
msgstr ""
"Aquí podreu configurar els grups. La llista desplegable té una selecció de "
"grups. El grup predeterminat estarà a la part superior."

#: ../../reference_manual/dockers/palette_docker.rst:64
msgid "Row Count"
msgstr "Nombre de files"

#: ../../reference_manual/dockers/palette_docker.rst:65
msgid ""
"The amount of rows in the group. If you want to add more colors to a group "
"and there's no empty areas to click on anymore, increase the row count."
msgstr ""
"La quantitat de files en el grup. Si voleu afegir més colors a un grup i ja "
"no hi ha cap àrea buida on fer clic, augmenteu el nombre de files."

#: ../../reference_manual/dockers/palette_docker.rst:66
msgid "Rename Group"
msgstr "Reanomena el grup"

#: ../../reference_manual/dockers/palette_docker.rst:67
msgid "Rename the group."
msgstr "Canviarà el nom del grup."

#: ../../reference_manual/dockers/palette_docker.rst:69
msgid ""
"Delete the group. It will ask whether you want to keep the colors. If so, it "
"will merge the group's contents with the default group."
msgstr ""
"Suprimirà el grup. Us demanarà si voleu mantenir els colors. Si és així, "
"fusionarà el contingut del grup amb el del grup predeterminat."

#: ../../reference_manual/dockers/palette_docker.rst:70
msgid "Group Settings"
msgstr "Ajustaments per als grups"

#: ../../reference_manual/dockers/palette_docker.rst:70
msgid "Delete Group"
msgstr "Suprimeix el grup"

#: ../../reference_manual/dockers/palette_docker.rst:72
msgid "The edit and new color dialogs ask for the following:"
msgstr "Els diàlegs edita i color nou demanen el següent:"

#: ../../reference_manual/dockers/palette_docker.rst:75
msgid "The color of the swatch."
msgstr "El color de la mostra."

#: ../../reference_manual/dockers/palette_docker.rst:76
msgid "Name"
msgstr "Nom"

#: ../../reference_manual/dockers/palette_docker.rst:77
msgid "The Name of the color in a human readable format."
msgstr "El nom del color en un format llegible per humans."

#: ../../reference_manual/dockers/palette_docker.rst:78
msgid "ID"
msgstr "ID"

#: ../../reference_manual/dockers/palette_docker.rst:79
msgid ""
"The ID is a number that can be used to index colors. Where Name can be "
"something like \"Pastel Peach\", ID will probably be something like "
"\"RY75\". Both names and ids can be used to search the color in the color "
"entry dropdown at the bottom of the palette."
msgstr ""
"L'ID és un número que es pot utilitzar per a l'índex del color. On Nom podrà "
"ser quelcom com «Pastís de préssec», l'ID probablement serà quelcom com "
"«RY75». Es poden utilitzar tant els noms com els identificadors per a cercar "
"el color a la llista desplegable d'entrada de color que hi ha a la part "
"inferior de la paleta."

#: ../../reference_manual/dockers/palette_docker.rst:81
msgid "Spot color"
msgstr "Color directe"

#: ../../reference_manual/dockers/palette_docker.rst:81
msgid ""
"Currently not used for anything within Krita itself, but spot colors are a "
"toggle to keep track of colors that represent a real world paint that a "
"printer can match. Keeping track of such colors is useful in a printing "
"workflow, and it can also be used with python to recognize spot colors."
msgstr ""
"Actualment no s'utilitza per a res dins del Krita, però els colors directes "
"són un alternador per a realitzar un seguiment dels colors que representen "
"una pintura en el món real que una impressora pot igualar. Fer un seguiment "
"d'aquests colors és útil en un flux de treball de la impressió, i també es "
"pot utilitzar amb Python per a reconèixer els colors directes."

#: ../../reference_manual/dockers/palette_docker.rst:83
msgid ""
"Krita's native palette format is since 4.0 :ref:`file_kpl`. It also supports "
"importing..."
msgstr ""
"Des de la versió 4.0, el format de la paleta natiu del Krita és el :ref:"
"`file_kpl`. També admet la importació..."

#: ../../reference_manual/dockers/palette_docker.rst:85
msgid "Gimp Palettes (.gpl)"
msgstr "Paletes del Gimp (.gpl)"

#: ../../reference_manual/dockers/palette_docker.rst:86
msgid "Microsoft RIFF palette (.riff)"
msgstr "Paleta RIFF de Microsoft (.riff)"

#: ../../reference_manual/dockers/palette_docker.rst:87
msgid "Photoshop Binary Palettes (.act)"
msgstr "Paletes binàries del Photoshop (.act)"

#: ../../reference_manual/dockers/palette_docker.rst:88
msgid "PaintShop Pro palettes (.psp)"
msgstr "Paletes del PaintShop Pro (.psp)"

#: ../../reference_manual/dockers/palette_docker.rst:89
msgid "Photoshop Swatches (.aco)"
msgstr "Mostres del Photoshop (.aco)"

#: ../../reference_manual/dockers/palette_docker.rst:90
msgid "Scribus XML (.xml)"
msgstr "En XML del Scribus (.xml)"

#: ../../reference_manual/dockers/palette_docker.rst:91
msgid "Swatchbooker (.sbz)."
msgstr "Swatchbooker (.sbz)."
