msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-27 08:31+0100\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../user_manual/getting_started.rst:5
msgid "Getting Started"
msgstr "Démarrage"

#: ../../user_manual/getting_started.rst:7
#, fuzzy
msgid ""
"Welcome to the Krita Manual! In this section, we'll try to get you up to "
"speed."
msgstr ""
"Bienvenue sur le manuel de Krita. Dans cette section, nous essaierons de "
"vous guider au mieux."

#: ../../user_manual/getting_started.rst:9
#, fuzzy
msgid ""
"If you are familiar with digital painting, we recommend checking out the :"
"ref:`introduction_from_other_software` category, which contains guides that "
"will help you get familiar with Krita by comparing its functions to other "
"software."
msgstr ""
"Si vous connaissez déjà la peinture numérique, nous vous recommandons : la "
"catégorie ref:`introduction_depuis_autre_logiciel` qui contient des guides "
"pour connaître les spécificités de Krita par rapport aux autres logiciels."

#: ../../user_manual/getting_started.rst:11
#, fuzzy
msgid ""
"If you are new to digital art, just start with :ref:`installation`, which "
"deals with installing Krita, and continue on to :ref:`starting_with_krita`, "
"which helps with making a new document and saving it, :ref:`basic_concepts`, "
"in which we'll try to quickly cover the big categories of Krita's "
"functionality, and finally, :ref:`navigation`, which helps you find basic "
"usage help, such as panning, zooming and rotating."
msgstr ""
"Si vous êtes débutant en peinture numérique, nous vous conseillons de "
"commencer avec :ref:`installation`, pour savoir comment bien installer "
"Krita, et de continuer avec :ref:`commencer_avec_krita`, expliquant comment "
"créer un premier document et l'enregistrer, :ref:`concepts_basiques`, dans "
"lequel nous tentons de couvrir les fonctionnalités principales de Krita, et "
"pour finir, :ref:`navigation`  indiquant l'utilisation de base du logiciel "
"comme se déplacer, zoomer et pivoter."

#: ../../user_manual/getting_started.rst:13
#, fuzzy
#| msgid ""
#| "When you have mastered those, you can look into the dedicated "
#| "introduction pages for functionality in the :ref:`user_manual`, read "
#| "through the over arching concepts behind (digital) painting in the :ref:"
#| "`general_concepts` section, or just search the :ref:`reference_manual` "
#| "for what a specific button does."
msgid ""
"When you have mastered those, you can look into the dedicated introduction "
"pages for functionality in the :ref:`user_manual`, read through the "
"overarching concepts behind (digital) painting in the :ref:"
"`general_concepts` section, or just search the :ref:`reference_manual` for "
"what a specific button does."
msgstr ""
"Une fois que tout cela est maîtrisé, nous vous invitons à regarder les pages "
"d'introduction dédiées des fonctionnalités dans :ref:`manuel_utilisation`, "
"pour en savoir plus sur les concepts de la peinture numérique lisez : ref:"
"`general_concepts`, ou cherchez dans :ref:`reference_manuel` l'usage "
"spécifique d'un bouton. "

#: ../../user_manual/getting_started.rst:15
msgid "Contents:"
msgstr "Contenu :"

#~ msgid ""
#~ ".. image:: images/icons/Krita_mouse_left.png\n"
#~ "   :alt: mouseleft"
#~ msgstr ""
#~ ".. image:: images/icons/Krita_mouse_left.png\n"
#~ "   :alt: clic gauche"

#~ msgid ""
#~ ".. image:: images/icons/Krita_mouse_right.png\n"
#~ "   :alt: mouseright"
#~ msgstr ""
#~ ".. image:: images/icons/Krita_mouse_right.png\n"
#~ "   :alt: clic droit"

#~ msgid ""
#~ ".. image:: images/icons/Krita_mouse_middle.png\n"
#~ "   :alt: mousemiddle"
#~ msgstr ""
#~ ".. image:: images/icons/Krita_mouse_middle.png\n"
#~ "   :alt: clic du milieu"

#~ msgid ""
#~ ".. image:: images/icons/Krita_mouse_scroll.png\n"
#~ "   :alt: mousescroll"
#~ msgstr ""
#~ ".. image:: images/icons/Krita_mouse_scroll.png\n"
#~ "   :alt: scroll"

#~ msgid ""
#~ ".. image:: images/icons/shape_select_tool.svg\n"
#~ "   :alt: toolshapeselection"
#~ msgstr ""
#~ ".. image:: images/icons/shape_select_tool.svg\n"
#~ "   :alt: outil se selection de forme"

#~ msgid ""
#~ ".. image:: images/icons/shape_edit_tool.svg\n"
#~ "   :alt: toolshapeedit"
#~ msgstr ""
#~ ".. image:: images/icons/shape_edit_tool.svg\n"
#~ "   :alt: outil d'édition de forme"

#~ msgid ""
#~ ".. image:: images/icons/text-tool.svg\n"
#~ "   :alt: tooltext"
#~ msgstr ""
#~ ".. image:: images/icons/text-tool.svg\n"
#~ "   :alt: outil texte"

#~ msgid ""
#~ ".. image:: images/icons/calligraphy_tool.svg\n"
#~ "   :alt: toolcalligraphy"
#~ msgstr ""
#~ ".. image:: images/icons/calligraphy_tool.svg\n"
#~ "   :alt: outil calligraphie"

#~ msgid ""
#~ ".. image:: images/icons/gradient_edit_tool.svg\n"
#~ "   :alt: toolgradientedit"
#~ msgstr ""
#~ ".. image:: images/icons/gradient_edit_tool.svg\n"
#~ "   :alt: outil d'édition de dégradé "

#~ msgid ""
#~ ".. image:: images/icons/pattern_tool.svg\n"
#~ "   :alt: toolpatternedit"
#~ msgstr ""
#~ ".. image:: images/icons/pattern_tool.svg\n"
#~ "   :alt: outil d'édition de motif"

#~ msgid ""
#~ ".. image:: images/icons/freehand_brush_tool.svg\n"
#~ "   :alt: toolfreehandbrush"
#~ msgstr ""
#~ ".. image:: images/icons/freehand_brush_tool.svg\n"
#~ "   :alt: outil de brosse libre"

#~ msgid ""
#~ ".. image:: images/icons/line_tool.svg\n"
#~ "   :alt: toolline"
#~ msgstr ""
#~ ".. image:: images/icons/line_tool.svg\n"
#~ "   :alt: outil ligne"

#~ msgid ""
#~ ".. image:: images/icons/rectangle_tool.svg\n"
#~ "   :alt: toolrectangle"
#~ msgstr ""
#~ ".. image:: images/icons/rectangle_tool.svg\n"
#~ "   :alt: outil rectangle"

#~ msgid ""
#~ ".. image:: images/icons/ellipse_tool.svg\n"
#~ "   :alt: toolellipse"
#~ msgstr ""
#~ ".. image:: images/icons/ellipse_tool.svg\n"
#~ "   :alt: outil ellipse"

#~ msgid ""
#~ ".. image:: images/icons/polygon_tool.svg\n"
#~ "   :alt: toolpolygon"
#~ msgstr ""
#~ ".. image:: images/icons/polygon_tool.svg\n"
#~ "   :alt: outil polygone "

#~ msgid ""
#~ ".. image:: images/icons/polyline_tool.svg\n"
#~ "   :alt: toolpolyline"
#~ msgstr ""
#~ ".. image:: images/icons/polyline_tool.svg\n"
#~ "   :alt: outil polyligne"

#~ msgid ""
#~ ".. image:: images/icons/bezier_curve.svg\n"
#~ "   :alt: toolbeziercurve"
#~ msgstr ""
#~ ".. image:: images/icons/bezier_curve.svg\n"
#~ "   :alt: outil courbe de bézier "

#~ msgid ""
#~ ".. image:: images/icons/freehand_path_tool.svg\n"
#~ "   :alt: toolfreehandpath"
#~ msgstr ""
#~ ".. image:: images/icons/freehand_path_tool.svg\n"
#~ "   :alt: outil libre de chemin"

#~ msgid ""
#~ ".. image:: images/icons/dyna_tool.svg\n"
#~ "   :alt: tooldyna"
#~ msgstr ""
#~ ".. image:: images/icons/dyna_tool.svg\n"
#~ "   :alt: outil dyna "

#~ msgid ""
#~ ".. image:: images/icons/multibrush_tool.svg\n"
#~ "   :alt: toolmultibrush"
#~ msgstr ""
#~ ".. image:: images/icons/multibrush_tool.svg\n"
#~ "   :alt: outil multibrosse"

#~ msgid ""
#~ ".. image:: images/icons/assistant_tool.svg\n"
#~ "   :alt: toolassistant"
#~ msgstr ""
#~ ".. image:: images/icons/assistant_tool.svg\n"
#~ "   :alt: outil assistant"

#~ msgid ""
#~ ".. image:: images/icons/move_tool.svg\n"
#~ "   :alt: toolmove"
#~ msgstr ""
#~ ".. image:: images/icons/move_tool.svg\n"
#~ "   :alt: outil déplacer"

#~ msgid ""
#~ ".. image:: images/icons/transform_tool.svg\n"
#~ "   :alt: tooltransform"
#~ msgstr ""
#~ ".. image:: images/icons/transform_tool.svg\n"
#~ "   :alt: outil transformation"

#~ msgid ""
#~ ".. image:: images/icons/grid_tool.svg\n"
#~ "   :alt: toolgrid"
#~ msgstr ""
#~ ".. image:: images/icons/grid_tool.svg\n"
#~ "   :alt: outil grille"

#~ msgid ""
#~ ".. image:: images/icons/perspectivegrid_tool.svg\n"
#~ "   :alt: toolperspectivegrid"
#~ msgstr ""
#~ ".. image:: images/icons/perspectivegrid_tool.svg\n"
#~ "   :alt: outil perspective"

#~ msgid ""
#~ ".. image:: images/icons/measure_tool.svg\n"
#~ "   :alt: toolmeasure"
#~ msgstr ""
#~ ".. image:: images/icons/measure_tool.svg\n"
#~ "   :alt: outil mesure"

#~ msgid ""
#~ ".. image:: images/icons/color_picker_tool.svg\n"
#~ "   :alt: toolcolorpicker"
#~ msgstr ""
#~ ".. image:: images/icons/color_picker_tool.svg\n"
#~ "   :alt: outil pipette"

#~ msgid ""
#~ ".. image:: images/icons/fill_tool.svg\n"
#~ "   :alt: toolfill"
#~ msgstr ""
#~ ".. image:: images/icons/fill_tool.svg\n"
#~ "   :alt: outil remplissage"

#~ msgid ""
#~ ".. image:: images/icons/gradient_drawing_tool.svg\n"
#~ "   :alt: toolgradient"
#~ msgstr ""
#~ ".. image:: images/icons/gradient_drawing_tool.svg\n"
#~ "   :alt: outil dégradé"

#~ msgid ""
#~ ".. image:: images/icons/colorizemask_tool.svg\n"
#~ "   :alt: toolcolorizemask"
#~ msgstr ""
#~ ".. image:: images/icons/colorizemask_tool.svg\n"
#~ "   :alt: outil masque de colorisation"

#~ msgid ""
#~ ".. image:: images/icons/smart_patch_tool.svg\n"
#~ "   :alt: toolsmartpatch"
#~ msgstr ""
#~ ".. image:: images/icons/smart_patch_tool.svg\n"
#~ "   :alt: outil clone intelligent"

#~ msgid ""
#~ ".. image:: images/icons/crop_tool.svg\n"
#~ "   :alt: toolcrop"
#~ msgstr ""
#~ ".. image:: images/icons/crop_tool.svg\n"
#~ "   :alt: outil découpage"

#~ msgid ""
#~ ".. image:: images/icons/rectangular_select_tool.svg\n"
#~ "   :alt: toolselectrect"
#~ msgstr ""
#~ ".. image:: images/icons/rectangular_select_tool.svg\n"
#~ "   :alt: outil sélection rectangulaire"

#~ msgid ""
#~ ".. image:: images/icons/elliptical_select_tool.svg\n"
#~ "   :alt: toolselectellipse"
#~ msgstr ""
#~ ".. image:: images/icons/elliptical_select_tool.svg\n"
#~ "   :alt: outil sélection elliptique"

#~ msgid ""
#~ ".. image:: images/icons/polygonal_select_tool.svg\n"
#~ "   :alt: toolselectpolygon"
#~ msgstr ""
#~ ".. image:: images/icons/polygonal_select_tool.svg\n"
#~ "   :alt: outil sélection polygonale"

#~ msgid ""
#~ ".. image:: images/icons/path_select_tool.svg\n"
#~ "   :alt: toolselectpath"
#~ msgstr ""
#~ ".. image:: images/icons/path_select_tool.svg\n"
#~ "   :alt: outil sélection de chemin"

#~ msgid ""
#~ ".. image:: images/icons/outline_select_tool.svg\n"
#~ "   :alt: toolselectoutline"
#~ msgstr ""
#~ ".. image:: images/icons/outline_select_tool.svg\n"
#~ "   :alt: outil selection de contour"

#~ msgid ""
#~ ".. image:: images/icons/contiguous_select_tool.svg\n"
#~ "   :alt: toolselectcontiguous"
#~ msgstr ""
#~ ".. image:: images/icons/contiguous_select_tool.svg\n"
#~ "   :alt: outil de sélection contigue"

#~ msgid ""
#~ ".. image:: images/icons/similar_select_tool.svg\n"
#~ "   :alt: toolselectsimilar"
#~ msgstr ""
#~ ".. image:: images/icons/similar_select_tool.svg\n"
#~ "   :alt: outil de sélection similaire"

#~ msgid ""
#~ ".. image:: images/icons/pan_tool.svg\n"
#~ "   :alt: toolpan"
#~ msgstr ""
#~ ".. image:: images/icons/pan_tool.svg\n"
#~ "   :alt: outil déplacement du canevas"

#~ msgid ""
#~ ".. image:: images/icons/zoom_tool.svg\n"
#~ "   :alt: toolzoom"
#~ msgstr ""
#~ ".. image:: images/icons/zoom_tool.svg\n"
#~ "   :alt: zoom"
