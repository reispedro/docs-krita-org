msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-02-27 01:33+0100\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../reference_manual/dockers/channels_docker.rst:1
msgid "Overview of the channels docker."
msgstr ""

#: ../../reference_manual/dockers/channels_docker.rst:10
msgid "Color"
msgstr ""

#: ../../reference_manual/dockers/channels_docker.rst:10
#, fuzzy
#| msgid "Channels"
msgid "Color Channels"
msgstr "Canaux"

#: ../../reference_manual/dockers/channels_docker.rst:15
msgid "Channels"
msgstr "Canaux"

#: ../../reference_manual/dockers/channels_docker.rst:18
msgid ".. image:: images/dockers/Krita_Channels_Docker.png"
msgstr ""

#: ../../reference_manual/dockers/channels_docker.rst:19
msgid ""
"The channel docker allows you to turn on and off the channels associated "
"with the color space that you are using. Each channel has an enabled and "
"disabled checkbox. You cannot edit individual layer channels from this "
"docker."
msgstr ""

#: ../../reference_manual/dockers/channels_docker.rst:22
msgid "Editing Channels"
msgstr ""

#: ../../reference_manual/dockers/channels_docker.rst:24
msgid ""
"If you want to edit individual channels by their grayscale component, you "
"will need to manually separate a layer. This can be done with a series of "
"commands with the layer docker."
msgstr ""

#: ../../reference_manual/dockers/channels_docker.rst:26
msgid "Select the layer you want to break apart."
msgstr ""

#: ../../reference_manual/dockers/channels_docker.rst:27
msgid "Go to :menuselection:`Image --> Separate Image`"
msgstr ""

#: ../../reference_manual/dockers/channels_docker.rst:28
msgid "Select the following options and click :guilabel:`OK`:"
msgstr ""

#: ../../reference_manual/dockers/channels_docker.rst:30
msgid "Source: Current Layer"
msgstr ""

#: ../../reference_manual/dockers/channels_docker.rst:31
msgid "Alpha Options: Create separate separation from alpha channel"
msgstr ""

#: ../../reference_manual/dockers/channels_docker.rst:32
msgid "Output to Grayscale, not color: unchecked"
msgstr ""

#: ../../reference_manual/dockers/channels_docker.rst:34
msgid "Hide your original layer"
msgstr ""

#: ../../reference_manual/dockers/channels_docker.rst:35
msgid ""
"Select All of the new channel layers and put them in a group layer (:"
"menuselection:`Layer --> Quick Group`)"
msgstr ""

#: ../../reference_manual/dockers/channels_docker.rst:36
msgid ""
"Select the Red layer and change the blending mode to \"Copy Red\" (these are "
"in the Misc. category)"
msgstr ""

#: ../../reference_manual/dockers/channels_docker.rst:37
msgid "Select the Green layer and change the blending mode  to \"Copy Green\""
msgstr ""

#: ../../reference_manual/dockers/channels_docker.rst:38
msgid "Select the Blue layer and change the blending mode to \"Copy Blue\""
msgstr ""

#: ../../reference_manual/dockers/channels_docker.rst:39
msgid "Make sure the Alpha layer is at the bottom of the group."
msgstr ""

#: ../../reference_manual/dockers/channels_docker.rst:40
msgid "Enable Inherit Alpha for the Red, Green, and Blue layers."
msgstr ""

#: ../../reference_manual/dockers/channels_docker.rst:42
msgid ""
"Here is a `video to see this process <https://www.youtube.com/watch?"
"v=lWuwegJ-mIQ&feature=youtu.be>`_ in Krita 3.0."
msgstr ""

#: ../../reference_manual/dockers/channels_docker.rst:44
msgid ""
"When working with editing channels, it can be easier to use the Isolate "
"Layer feature to only see the channel. Right-click on the layer to find "
"Isolate Layer."
msgstr ""
