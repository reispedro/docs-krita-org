# Translation of docs_krita_org_user_manual___animation.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_user_manual___animation\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:39+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../user_manual/animation.rst:1
msgid "Detailed guide on the animation workflow in Krita."
msgstr "Докладні настанови із роботи з анімацією у Krita."

#: ../../user_manual/animation.rst:13
msgid "Animation"
msgstr "Анімація"

#: ../../user_manual/animation.rst:18
msgid "Animation with Krita"
msgstr "Створення анімації за допомогою Krita"

#: ../../user_manual/animation.rst:20
msgid ""
"Thanks to the 2015 Kickstarter, :program:`Krita 3.0` now has animation. In "
"specific, :program:`Krita` has frame-by-frame raster animation. There's "
"still a lot of elements missing from it, like tweening, but the basic "
"workflow is there."
msgstr ""
"Завдяки кампанії 2015 року у Kickstarter, у :program:`Krita 3.0` з'явилися "
"можливості анімації. Якщо бути точнішим, у :program:`Krita` тепер є "
"покадрова растрова анімація. У ній усе ще не вистачає багатьох елементів, "
"зокрема проміжних кадрів, але базова частина уже на місці."

#: ../../user_manual/animation.rst:25
msgid ""
"To access the animation features, the easiest way is to change your "
"workspace to Animation. This will make the animation dockers and workflow "
"appear."
msgstr ""
"Найпростішим способом дістатися можливостей анімації є перемикання робочого "
"простору у режимі «Анімація». У відповідь буде показано бічні панелі та інші "
"інструменти анімації."

#: ../../user_manual/animation.rst:30
msgid "Animation curves"
msgstr "Криві анімації"

#: ../../user_manual/animation.rst:32
msgid ""
"To create an animation curve (currently only for opacity) expand the :"
"guilabel:`New Frame` button in the :guilabel:`Animation` dock and click :"
"guilabel:`Add Opacity Keyframe`. You can now edit the keyframed value for "
"opacity directly in the “Layers” dock, adding more keyframes will by default "
"fade from the last to the next upcoming keyframe in the timeline over the "
"frames between them. See :ref:`animation curves <animation_curves_docker>` "
"for details."
msgstr ""
"Щоб створити криву анімації (у поточній версії лише для непрозорості), "
"натисніть кнопку :guilabel:`Створити кадр` на бічній панелі :guilabel:"
"`Анімація` і натисніть кнопку :guilabel:`Додати ключовий кадр непрозорості`. "
"Після цього ви зможете редагувати значення непрозорості для ключового кадру "
"безпосередньо на бічній панелі «Шари». Додавання нових ключових кадрів, "
"типово, призводитиме до згасання інтенсивності кольорів інших ключових "
"кадрів, пропорційно до віддаленості кадрів від поточного. Докладніший опис "
"можна знайти у розділі щодо :ref:`кривих анімації <animation_curves_docker>`."

#: ../../user_manual/animation.rst:40
msgid "Workflow"
msgstr "Процес"

#: ../../user_manual/animation.rst:42
msgid ""
"In traditional animation workflow, what you do is that you make *key "
"frames*, which contain the important poses, and then draw frames in between "
"(\\ *tweening* in highly sophisticated animator's jargon)."
msgstr ""
"У традиційній анімації ви створюєте «ключові кадри», які містять важливі "
"пози персонажів, а потім домальовуєте проміжні кадри."

#: ../../user_manual/animation.rst:46
msgid "For this workflow, there are three important dockers:"
msgstr "Для виконання цих дій передбачено три важливі бічні панелі:"

#: ../../user_manual/animation.rst:48
msgid ""
"The :ref:`timeline_docker`. View and control all of the frames in your "
"animation. The timeline docker also contains functions to manage your "
"layers. The layer that are created in the timeline docker also appear on the "
"normal Layer docker."
msgstr ""
":ref:`timeline_docker`. Перегляд і керування усіма кадрами у вашій анімації. "
"На бічній панелі монтажного столу також містяться елементи для керування "
"шарами. Шар, який буде створено на бічній панелі монтажного столу, з'явиться "
"і у переліку шарів звичайної бічної панелі шарів."

#: ../../user_manual/animation.rst:52
msgid ""
"The :ref:`animation_docker`. This docker contains the play buttons as the "
"ability to change the frame-rate, playback speed and useful little options "
"like :guilabel:`auto-key framing`."
msgstr ""
":ref:`animation_docker`. На цій панелі містяться кнопки відтворення, віджет "
"керування частотою кадрів, швидкістю відтворення та корисні пункти, зокрема "
"пункт автоматично додавання ключових кадрів."

#: ../../user_manual/animation.rst:55
msgid ""
"The :ref:`onion_skin_docker`. This docker controls the look of the onion "
"skin, which in turn is useful for seeing the previous frame."
msgstr ""
":ref:`onion_skin_docker`. За допомогою цієї бічної панелі можна керувати "
"виглядом кальки, яка корисна для перегляду попередніх кадрів."

#: ../../user_manual/animation.rst:60
msgid "Introduction to animation: How to make a walkcycle"
msgstr "Вступ до анімування: створюємо модель, яка йтиме"

#: ../../user_manual/animation.rst:62
msgid ""
"The best way to get to understand all these different parts is to actually "
"use them. Walk cycles are considered the most basic form of a full "
"animation, because of all the different parts involved with them. Therefore, "
"going over how one makes a walkcycle should serve as a good introduction."
msgstr ""
"Найкращим способом зрозуміти призначення усіх цих частин є скористатися "
"ними. Цикл кроків вважається найпростішою формою повноцінної анімації, "
"оскільки до нього включено усі частини анімації. Тому виконання усіх дій, "
"які потрібні для створення анімації кроків, має бути добрим вступним "
"підручником щодо використання анімації."

#: ../../user_manual/animation.rst:69
msgid "Setup"
msgstr "Налаштування"

#: ../../user_manual/animation.rst:71
msgid "First, we make a new file:"
msgstr "Спершу, створімо новий файл:"

#: ../../user_manual/animation.rst:74
msgid ".. image:: images/animation/Introduction_to_animation_01.png"
msgstr ".. image:: images/animation/Introduction_to_animation_01.png"

#: ../../user_manual/animation.rst:75
msgid ""
"On the first tab, we type in a nice ratio like 1280x1024, set the dpi to 72 "
"(we're making this for screens after all) and title the document 'walkcycle'."
msgstr ""
"На першій вкладці вводимо розміри зображення, наприклад 1280x1024, "
"встановлюємо роздільність у 72 точки на дюйм (ми все одно робимо цю анімацію "
"для перегляду на екрані комп'ютера) і вказуємо назву документа — «walkcycle»."

#: ../../user_manual/animation.rst:79
msgid ""
"In the second tab, we choose a nice background color, and set the background "
"to canvas-color. This means that Krita will automatically fill in any "
"transparent bits with the background color. You can change this in :"
"menuselection:`Image --> Image Properties`. This seems to be most useful to "
"people doing animation, as the layer you do animation on MUST be semi-"
"transparent to get onion skinning working."
msgstr ""
"На другій вкладці вибираємо колір тла і встановлюємо для кольору тла колір "
"полотна. Це означає, що Krita автоматично заповнюватиме усі прозорі ділянки "
"кольором тла. Змінити це можна за допомогою пункту меню :menuselection:"
"`Зображення --> Властивості зображення`. Цей варіант є найкориснішим для "
"анімації, оскільки шар, на якому ви малюєте анімацію, *має* бути прозорим, "
"щоб можна було скористатися калькою."

#: ../../user_manual/animation.rst:82
msgid ""
"Krita has a bunch of functionality for meta-data, starting at the :guilabel:"
"`Create Document` screen. The title will be automatically used as a "
"suggestion for saving and the description can be used by databases, or for "
"you to leave comments behind. Not many people use it individually, but it "
"can be useful for working in larger groups."
msgstr ""
"У Krita передбачено декілька інструментів для роботи із метаданими, "
"починаючи з вікна :guilabel:`Створити документ`. Заголовок документа буде "
"автоматично використано як пропозицію для назви файла для зберігання, а опис "
"може бути використано у базах даних або для ваших коментарів щодо анімації. "
"На індивідуальному рівні мало хто користується описами, але вони можуть бути "
"корисними, якщо ви працюєте у великій групі."

#: ../../user_manual/animation.rst:84
msgid "Then hit :guilabel:`Create`!"
msgstr "Далі, просто натисніть :guilabel:`Створити`!"

#: ../../user_manual/animation.rst:86
msgid ""
"Then, to get all the necessary tools for animation, select the workspace "
"switcher:"
msgstr ""
"Щоб отримати доступ до усіх потрібних інструментів анімації, виберіть "
"перемикач робочих просторів:"

#: ../../user_manual/animation.rst:91
msgid ".. image:: images/animation/Introduction_to_animation_02.png"
msgstr ".. image:: images/animation/Introduction_to_animation_02.png"

#: ../../user_manual/animation.rst:91
msgid "The red arrow points at the workspace switcher."
msgstr "Червона стрілка вказує на перемикач робочих просторів."

#: ../../user_manual/animation.rst:93
msgid "And select the animation workspace."
msgstr "І виберіть робочий простір анімації."

#: ../../user_manual/animation.rst:95
msgid "Which should result in this:"
msgstr "Результат має бути якимось таким:"

#: ../../user_manual/animation.rst:98
msgid ".. image:: images/animation/Introduction_to_animation_03.png"
msgstr ".. image:: images/animation/Introduction_to_animation_03.png"

#: ../../user_manual/animation.rst:99
msgid ""
"The animation workspace adds the timeline, animation and onion skin dockers "
"at the bottom."
msgstr ""
"У робочому просторі анімації у нижній частині додається монтажний стіл і "
"бічні панелі анімації та кальки."

#: ../../user_manual/animation.rst:103
msgid "Animating"
msgstr "Анімування"

#: ../../user_manual/animation.rst:105
msgid ""
"We have two transparent layers set up. Let's name the bottom one "
"'environment' and the top 'walkcycle' by double clicking their names in the "
"layer docker."
msgstr ""
"Нами налаштовано два прозорих шари. Назвімо нижній шар "
"«environment» (середовище), а верхній — «walkcycle» («цикл крокування»). Щоб "
"розпочати редагування назв, двічі клацніть на пункті назви на бічній панелі "
"шарів."

#: ../../user_manual/animation.rst:110
msgid ".. image:: images/animation/Introduction_to_animation_04.png"
msgstr ".. image:: images/animation/Introduction_to_animation_04.png"

#: ../../user_manual/animation.rst:111
msgid ""
"Use the straight line tool to draw a single horizontal line. This is the "
"ground."
msgstr ""
"Скористайтеся інструментом для малювання прямих, щоб намалювати одну "
"горизонтальну лінію. Це буде наша підлога."

#: ../../user_manual/animation.rst:115
msgid ".. image:: images/animation/Introduction_to_animation_05.png"
msgstr ".. image:: images/animation/Introduction_to_animation_05.png"

#: ../../user_manual/animation.rst:116
msgid ""
"Then, select the 'walkcycle' layer and draw a head and torso (you can use "
"any brush for this)."
msgstr ""
"Далі, виберіть шар «walkcycle» і намалюйте голову і тулуб (можете "
"скористатися для цього будь-яким пензлем)."

#: ../../user_manual/animation.rst:118
msgid ""
"Now, selecting a new frame will not make a new frame automatically. Krita "
"doesn't actually see the 'walkcycle' layer as an animated layer at all!"
msgstr ""
"Тепер, якщо вибрати пункт створення нового кадру, програма не створить новий "
"кадр автоматично. Насправді, Krita не бачить шар «walkcycle» як анімований "
"шар взагалі!"

#: ../../user_manual/animation.rst:123
msgid ".. image:: images/animation/Introduction_to_animation_06.png"
msgstr ".. image:: images/animation/Introduction_to_animation_06.png"

#: ../../user_manual/animation.rst:124
msgid ""
"We can make it animatable by adding a frame to the timeline. |mouseright| a "
"frame in the timeline to get a context menu. Choose :guilabel:`New Frame`."
msgstr ""
"Ми можемо зробити шар анімованим, додавши кадр на монтажний стіл. Клацніть |"
"mouseright| на кадрі на монтажному столі, щоб викликати контекстне меню. "
"Виберіть у контекстному меню пункт :guilabel:`Створити кадр`."

#: ../../user_manual/animation.rst:128
msgid ".. image:: images/animation/Introduction_to_animation_07.png"
msgstr ".. image:: images/animation/Introduction_to_animation_07.png"

#: ../../user_manual/animation.rst:129
msgid ""
"You can see it has become an animated layer because of the onion skin icon "
"showing up in the timeline docker."
msgstr ""
"Як можете бачити, кадр став анімованим шаром, бо програма показала поряд із "
"його пунктом піктограму кальки на бічній панелі монтажного столу."

#: ../../user_manual/animation.rst:133
msgid ".. image:: images/animation/Introduction_to_animation_08.png"
msgstr ".. image:: images/animation/Introduction_to_animation_08.png"

#: ../../user_manual/animation.rst:134
msgid ""
"Use the :guilabel:`Copy Frame` button to copy the first frame onto the "
"second. Then, use the with the :kbd:`Shift + ↑` shortcut to move the frame "
"contents up."
msgstr ""
"Натисніть кнопку :guilabel:`Копіювати кадр` для копіювання першого кадру до "
"другого. Далі, скористайтеся комбінацією клавіш :kbd:`Shift + ↑`, щоб "
"підняти вміст кадру вище."

#: ../../user_manual/animation.rst:137
msgid "We can see the difference by turning on the onionskinning:"
msgstr "Різницю можна бачити, якщо увімкнути кальку:"

#: ../../user_manual/animation.rst:140
msgid ".. image:: images/animation/Introduction_to_animation_09.png"
msgstr ".. image:: images/animation/Introduction_to_animation_09.png"

#: ../../user_manual/animation.rst:141
msgid "Now, you should see the previous frame as red."
msgstr "Тепер, попередній кадр позначено червоним кольором."

#: ../../user_manual/animation.rst:144
msgid ""
"Krita sees white as a color, not as transparent, so make sure the animation "
"layer you are working on is transparent in the bits where there's no "
"drawing. You can fix the situation by use the :ref:`filter_color_to_alpha` "
"filter, but prevention is best."
msgstr ""
"Krita вважає ділянки, які зафарбовано білим кольором, білими, а не "
"прозорими, отже вам слід переконатися, що шар анімації, над яким ви "
"працюєте, є прозорим там, де нічого не намальовано. Виправити ситуацію можна "
"за допомогою фільтра :ref:`filter_color_to_alpha`, але краще взагалі "
"запобігти появі цієї проблеми."

#: ../../user_manual/animation.rst:147
msgid ".. image:: images/animation/Introduction_to_animation_10.png"
msgstr ".. image:: images/animation/Introduction_to_animation_10.png"

#: ../../user_manual/animation.rst:148
msgid ""
"Future frames are drawn in green, and both colors can be configured in the "
"onion skin docker."
msgstr ""
"Майбутні кадри буде намальовано зеленим. Обидва кольори можна налаштувати за "
"допомогою бічної панелі кальки."

#: ../../user_manual/animation.rst:152
msgid ".. image:: images/animation/Introduction_to_animation_11.png"
msgstr ".. image:: images/animation/Introduction_to_animation_11.png"

#: ../../user_manual/animation.rst:153
msgid ""
"Now, we're gonna draw the two extremes of the walkcycle. These are the pose "
"where both legs are as far apart as possible, and the pose where one leg is "
"full stretched and the other pulled in, ready to take the next step."
msgstr ""
"Тепер, намалюймо два крайніх стани під час крокування. Це поза, коли обидві "
"ступні перебувають на максимальній відстані одна від одної, і поза, коли "
"одну ногу повністю витягнуто, а друга стоїть на підлозі, готова до "
"наступного кроку."

#: ../../user_manual/animation.rst:158
msgid ""
"Now, let's copy these two... We could do that with the :kbd:`Ctrl + drag` "
"shortcut, but here comes a tricky bit:"
msgstr ""
"Тепер, скопіюймо ці два кадри… Це можна було б зробити за допомогою "
"комбінації :kbd:`Ctrl + перетягування`, але тут є тонкощі:"

#: ../../user_manual/animation.rst:162
msgid ".. image:: images/animation/Introduction_to_animation_12.png"
msgstr ".. image:: images/animation/Introduction_to_animation_12.png"

#: ../../user_manual/animation.rst:163
msgid ""
":kbd:`Ctrl +` |mouseleft| also selects and deselects frames, so to copy..."
msgstr ""
"Крім того, :kbd:`Ctrl +` |mouseleft| позначає і скасовує позначення кадрів; "
"отже, для копіювання…"

#: ../../user_manual/animation.rst:165
msgid ":kbd:`Ctrl +` |mouseleft| to select all the frames you want to select."
msgstr ""
"Скористайтеся :kbd:`Ctrl +` |mouseleft| для позначення усіх потрібних вам "
"кадрів."

#: ../../user_manual/animation.rst:166
msgid ""
":kbd:`Ctrl + drag`. You need to make sure the first frame is 'orange', "
"otherwise it won't be copied along."
msgstr ""
"Скористайтеся комбінацією :kbd:`Ctrl + перетягування`. Вам слід "
"переконатися, що перший кадр є «помаранчевим», інакше його не буде "
"скопійовано."

#: ../../user_manual/animation.rst:169
msgid "Now then..."
msgstr "Далі…"

#: ../../user_manual/animation.rst:174
msgid ".. image:: images/animation/Introduction_to_animation_13.png"
msgstr ".. image:: images/animation/Introduction_to_animation_13.png"

#: ../../user_manual/animation.rst:174
msgid "squashed the timeline docker a bit to save space"
msgstr "Згортання бічної панелі монтажного столу заощадить трохи місця"

#: ../../user_manual/animation.rst:176
msgid "Copy frame 0 to frame 2."
msgstr "Скопіюйте кадр 0 до кадру 2."

#: ../../user_manual/animation.rst:177
msgid "Copy frame 1 to frame 3."
msgstr "Скопіюйте кадр 1 до кадру 3."

#: ../../user_manual/animation.rst:178
msgid "In the animation docker, set the frame-rate to 4."
msgstr "На бічній панелі анімації встановіть частоту кадрів 4."

#: ../../user_manual/animation.rst:179
msgid "Select all frames in the timeline docker by dragging-selecting them."
msgstr "Позначте усі кадри на бічній панелі монтажного столу перетягуванням."

#: ../../user_manual/animation.rst:180
msgid "Press play in the animation docker."
msgstr "Натисніть кнопку відтворення на бічній панелі анімації."

#: ../../user_manual/animation.rst:181
msgid "Enjoy your first animation!"
msgstr "Насолоджуйтеся вашою першою анімацією!"

#: ../../user_manual/animation.rst:184
msgid "Expanding upon your rough walkcycle"
msgstr "Удосконалюємо наше жорстке крокування"

#: ../../user_manual/animation.rst:187
msgid ".. image:: images/animation/Introduction_to_animation_14.png"
msgstr ".. image:: images/animation/Introduction_to_animation_14.png"

#: ../../user_manual/animation.rst:188
msgid ""
"You can quickly make some space by the :kbd:`Alt + drag` shortcut on any "
"frame. This'll move that frame and all others after it in one go."
msgstr ""
"Швидко додати трохи місця можна за допомогою комбінації :kbd:`Alt + "
"перетягування` на будь-якому кадрі. Таким чином можна переміщувати кадр та "
"усі інші кадри за ним одним рухом."

#: ../../user_manual/animation.rst:191
msgid "Then draw inbetweens on each frame that you add."
msgstr "Тепер намалюйте проміжні кадри для кожного доданого кадру."

#: ../../user_manual/animation.rst:194
msgid ".. image:: images/animation/Introduction_to_animation_16.png"
msgstr ".. image:: images/animation/Introduction_to_animation_16.png"

#: ../../user_manual/animation.rst:195
msgid ""
"You'll find that the more frames you add, the more difficult it becomes to "
"keep track of the onion skins."
msgstr ""
"Ви могли помітити, що чим більше стає кадрів, тим складніше стежити за ними "
"за допомогою кальки."

#: ../../user_manual/animation.rst:197
msgid ""
"You can modify the onion skin by using the onion skin docker, where you can "
"change how many frames are visible at once, by toggling them on the top row. "
"The bottom row is for controlling transparency, while below there you can "
"modify the colors and extremity of the coloring."
msgstr ""
"Змінити параметри кальки можна за допомогою бічної панелі кальки. Там можна "
"визначити кількість кадрів, які буде показано одночасно, клацанням на "
"номерах кадрів у верхньому рядку. За допомогою нижнього рядка можна керувати "
"прозорістю. У нижній частині панелі розташовано віджети керування "
"прозорістю, кольорів кадрів та яскравості розфарбовування."

#: ../../user_manual/animation.rst:203
msgid ".. image:: images/animation/Introduction_to_animation_15.png"
msgstr ".. image:: images/animation/Introduction_to_animation_15.png"

#: ../../user_manual/animation.rst:205
msgid "Animating with multiple layers"
msgstr "Анімація із декількома шарами"

#: ../../user_manual/animation.rst:207
msgid ""
"Okay, our walkcycle is missing some hands, let's add them on a separate "
"layer. So we make a new layer, and name it hands and..."
msgstr ""
"Гаразд, у нашого чоловічка не вистачає рук. Додамо їх на окремому шарі. "
"Створімо шар, назвімо його «hands» і…"

#: ../../user_manual/animation.rst:211
msgid ".. image:: images/animation/Introduction_to_animation_17.png"
msgstr ".. image:: images/animation/Introduction_to_animation_17.png"

#: ../../user_manual/animation.rst:212
msgid ""
"Our walkcycle is gone from the timeline docker! This is a feature actually. "
"A full animation can have so many little parts that an animator might want "
"to remove the layers they're not working on from the timeline docker. So you "
"manually have to add them."
msgstr ""
"Наше крокування зникло з бічної панелі монтажного столу! Насправді, це "
"просто особливість робочого процесу. У повноцінній анімації може бути так "
"багато частин, що може виникнути потреба в усуванні шарів, над якими ви не "
"працюєте з бічної панелі монтажного столу. Отже, вам слід додати потрібні "
"вам шари вручну."

#: ../../user_manual/animation.rst:218
msgid ".. image:: images/animation/Introduction_to_animation_18.png"
msgstr ".. image:: images/animation/Introduction_to_animation_18.png"

#: ../../user_manual/animation.rst:219
msgid ""
"You can show any given layer in the timeline by doing |mouseright| on the "
"layer in the layer docker, and toggling :guilabel:`Show in Timeline`."
msgstr ""
"Наказати програмі показувати певний шар на панелі монтажного столу можна "
"клацнувши |mouseright| на пункті шару на бічній панелі шарів і вибравши "
"пункт :guilabel:`Показати на монтажному столі`."

#: ../../user_manual/animation.rst:223
msgid ".. image:: images/animation/Introduction_to_animation_19.png"
msgstr ".. image:: images/animation/Introduction_to_animation_19.png"

#: ../../user_manual/animation.rst:225
msgid "Exporting"
msgstr "Експортування"

#: ../../user_manual/animation.rst:227
msgid "When you are done, select :menuselection:`File --> Render Animation`."
msgstr ""
"Коли роботу буде завершено, скористайтеся пунктом меню :menuselection:`Файл "
"--> Обробити анімацію`."

#: ../../user_manual/animation.rst:230
msgid ".. image:: images/animation/Introduction_to_animation_20.png"
msgstr ".. image:: images/animation/Introduction_to_animation_20.png"

#: ../../user_manual/animation.rst:231
msgid ""
"It's recommended to save out your file as a png, and preferably in its own "
"folder. Krita can currently only export png sequences."
msgstr ""
"Рекомендуємо зберегти ваш файл у форматі png і, бажано, у окремій теці. У "
"поточній версії Krita передбачено експортування послідовності зображень лише "
"у форматі png."

#: ../../user_manual/animation.rst:235
msgid ".. image:: images/animation/Introduction_to_animation_21.png"
msgstr ".. image:: images/animation/Introduction_to_animation_21.png"

#: ../../user_manual/animation.rst:236
msgid ""
"When pressing done, you can see the status of the export in the status bar "
"below."
msgstr ""
"Після натискання кнопки :guilabel:`Гаразд` ви побачите статус експортування "
"на смужці стану у нижній частині вікна."

#: ../../user_manual/animation.rst:240
msgid ".. image:: images/animation/Introduction_to_animation_22.png"
msgstr ".. image:: images/animation/Introduction_to_animation_22.png"

#: ../../user_manual/animation.rst:241
msgid ""
"The images should be saved out as filenameXXX.png, giving their frame number."
msgstr ""
"Зображення має бути збережено як filenameXXX.png, де «XXX» номер кадру."

#: ../../user_manual/animation.rst:244
msgid ""
"Then use something like Gimp (Linux, OSX, Windows), ImageMagick (Linux, OSX, "
"Windows), or any other gif creator to make a gif out of your image sequence:"
msgstr ""
"Далі, скористайтеся чимось на штиб GIMP (Linux, OSX, Windows), ImageMagick "
"(Linux, OSX, Windows) або якоюсь іншою програмою для створення gif, щоб "
"отримати gif з послідовності зображень:"

#: ../../user_manual/animation.rst:249
msgid ".. image:: images/animation/Introduction_to_animation_walkcycle_02.gif"
msgstr ".. image:: images/animation/Introduction_to_animation_walkcycle_02.gif"

#: ../../user_manual/animation.rst:250
msgid ""
"For example, you can use `VirtualDub <http://www.virtualdub.org/>`_\\ "
"(Windows) and open all the frames and then go to :menuselection:`File --> "
"Export --> GIF`."
msgstr ""
"Наприклад, ви можете скористатися `VirtualDub <http://www.virtualdub.org/>`__"
"\\ (Windows), відкрити усі кадри і скористатися пунктом меню :menuselection:"
"`File --> Export --> GIF`."

#: ../../user_manual/animation.rst:254
msgid "Enjoy your walkcycle!"
msgstr "Насолоджуйтеся вашою анімацією крокування!"

#: ../../user_manual/animation.rst:258
msgid ""
"Krita 3.1 has a render animation feature. If you're using the 3.1 beta, "
"check out the :ref:`render_animation` page for more information!"
msgstr ""
"У Krita 3.1 передбачено можливість створення готових анімацій. Якщо ви "
"користуєтеся 3.1 beta або новішою версією, ознайомтеся зі сторінкою :ref:"
"`render_animation`, щоб дізнатися більше!"

#: ../../user_manual/animation.rst:261
msgid "Importing animation frames"
msgstr "Імпортування кадрів анімації"

#: ../../user_manual/animation.rst:263
msgid "You can import animation frames in Krita 3.0."
msgstr "У Krita 3.0 ви можете імпортувати кадри анімації."

#: ../../user_manual/animation.rst:265
msgid ""
"First let us take a sprite sheet from Open Game Art. (This is the Libre "
"Pixel Cup male walkcycle)"
msgstr ""
"Спочатку слід отримати набір спрайтів з Open Game Art. (Це крокування "
"чоловічка з Libre Pixel Cup)"

#: ../../user_manual/animation.rst:268
msgid ""
"And we'll use :menuselection:`Image --> Split Image` to split up the sprite "
"sheet."
msgstr ""
"Далі, скористаймося пунктом меню :menuselection:`Зображення --> Поділ "
"зображень`, щоб розділити набір спрайтів."

#: ../../user_manual/animation.rst:271
msgid ".. image:: images/animation/Animation_split_spritesheet.png"
msgstr ".. image:: images/animation/Animation_split_spritesheet.png"

#: ../../user_manual/animation.rst:272
msgid ""
"The slices are even, so for a sprite sheet of 9 sprites, use 8 vertical "
"slices and 0 horizontal slices. Give it a proper name and save it as png."
msgstr ""
"Спрайти розподілено рівномірно, тому для набору з 9 спрайтів скористайтеся 8 "
"вертикальними розрізами та 0 горизонтальних розрізів. Надайте документу "
"відповідної назви і збережіть його як png."

#: ../../user_manual/animation.rst:274
msgid ""
"Then, make a new canvas, and select :menuselection:`File --> Import "
"Animation Frames`. This will give you a little window. Select :guilabel:`Add "
"images`. This should get you a file browser where you can select your images."
msgstr ""
"Далі, створіть нове полотно і виберіть у меню пункт :menuselection:`Файл --> "
"Імпортувати кадри анімації`. У відповідь буде відкрито невеличке вікно. "
"Натисніть кнопку :guilabel:`Додати зображення`. У відповідь буде відкрито "
"вікно навігації файловою системою, за допомогою якого ви зможете вибрати "
"ваші зображення."

#: ../../user_manual/animation.rst:277
msgid ".. image:: images/animation/Animation_import_sprites.png"
msgstr ".. image:: images/animation/Animation_import_sprites.png"

#: ../../user_manual/animation.rst:278
msgid "You can select multiple images at once."
msgstr "Ви можете позначити декілька зображень одночасно."

#: ../../user_manual/animation.rst:281
msgid ".. image:: images/animation/Animation_set_everything.png"
msgstr ".. image:: images/animation/Animation_set_everything.png"

#: ../../user_manual/animation.rst:282
msgid ""
"The frames are currently automatically ordered. You can set the ordering "
"with the top-left two drop-down boxes."
msgstr ""
"Тепер кадри має бути автоматично упорядковано. Ви можете встановити "
"упорядкування за допомогою двох спадних списків угорі ліворуч."

#: ../../user_manual/animation.rst:285
msgid "Start"
msgstr "Початок"

#: ../../user_manual/animation.rst:286
msgid "Indicates at which point the animation should be imported."
msgstr "Вказує, у якій точці має бути імпортовано анімацію."

#: ../../user_manual/animation.rst:288
msgid ""
"Indicates the difference between the imported animation and the document "
"frame rate. This animation is 8 frames big, and the fps of the document is "
"24 frames, so there should be a step of 3 to keep it even. As you can see, "
"the window gives feedback on how much fps the imported animation would be "
"with the currently given step."
msgstr ""
"Вказує на відмінність між імпортованою анімацією та частотою кадрів "
"документа. Наша анімація складається з 8 кадрів, а частота кадрів у "
"документі дорівнює 24 кадрам за секунду. Отже, що розподілити кадри "
"рівномірно, крок має дорівнювати 3. Як бачите, у вікні буде показано дані "
"щодо того, якою буде частота кадрів імпортованої анімації за поточного "
"вибраного кроку."

#: ../../user_manual/animation.rst:292
msgid "Step"
msgstr "Крок"

#: ../../user_manual/animation.rst:294
msgid ""
"Press :guilabel:`OK`, and your animation should be imported as a new layer."
msgstr ""
"Натисніть кнопку :guilabel:`Гаразд`, і програма імпортує вашу анімацію як "
"новий шар."

#: ../../user_manual/animation.rst:297
msgid ".. image:: images/animation/Animation_import_done.png"
msgstr ".. image:: images/animation/Animation_import_done.png"

#: ../../user_manual/animation.rst:299
msgid "Reference"
msgstr "Посилання"

#: ../../user_manual/animation.rst:301
msgid "https://community.kde.org/Krita/Docs/AnimationGuiFeaturesList"
msgstr "https://community.kde.org/Krita/Docs/AnimationGuiFeaturesList"

#: ../../user_manual/animation.rst:302
msgid ""
"`The source for the libre pixel cup male walkmediawiki cycle <https://"
"opengameart.org/content/liberated-pixel-cup-lpc-base-assets-sprites-map-"
"tiles>`_"
msgstr ""
"`Початковий набір спрайтів для крокування чоловічка libre pixel cup <https://"
"opengameart.org/content/liberated-pixel-cup-lpc-base-assets-sprites-map-"
"tiles>`_"
